import { Action } from '@ngrx/store';
import { Letter } from './letters.reducer';
import { LettersActions, LettersActionTypes } from '../actions/letters.actions';

export interface Letter {
  id: number;
  value: string;
  x: number;
  y: number;
}

export type State = Letter[];

export const initialState: State = [
  {id: 1, value: 'b', x: 5, y: 9}
];

export function reducer(state = initialState, action: LettersActions): State {
  switch (action.type) {
    case LettersActionTypes.Create:
      return [ ...state, action.payload ];
    case LettersActionTypes.Destroy:
      return state.filter(letter => letter.id !== action.payload);
    case LettersActionTypes.Move:
      return state.map(letter => {
        const updated: Letter = {
          id: letter.id,
          value: letter.value,
          x: letter.x,
          y: letter.y + 1
        };
        return updated;
      });
    default:
      return state;
  }
}
