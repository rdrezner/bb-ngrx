import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import { State as Letters } from './letters.reducer';
import { reducer as letters } from './letters.reducer';

export interface State {
  letters: Letters;
}

export const reducers: ActionReducerMap<State> = {
  letters
};

export const metaReducers: MetaReducer<State>[] = !environment.production
  ? []
  : [];
