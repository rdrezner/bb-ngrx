import { Component, OnInit, Input } from '@angular/core';
import { Letter } from '../reducers/letters.reducer';

@Component({
  selector: 'app-field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.css']
})
export class FieldComponent implements OnInit {
  @Input() letters: Letter[];

  constructor() {}

  ngOnInit() {}
}
