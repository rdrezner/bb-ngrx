import { Component, OnInit, HostListener } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { timer } from 'rxjs/observable/timer';
import { map, scan, tap, startWith, merge, mapTo } from 'rxjs/operators';
import {Subject} from 'rxjs/Subject';

interface Action {
  type: string;
  payload: any;
}

@Component({
  selector: 'app-by-hand',
  templateUrl: './by-hand.component.html',
  styleUrls: ['./by-hand.component.css']
})
export class ByHandComponent implements OnInit {

  points$: Observable<{hits: number, misses: number}>;
  click$ = new Subject();

  constructor() {
    this.points$ = timer(0, 1000).pipe(mapTo({type: 'HIT'}))
      .pipe(
        // INITIAL STATE
        startWith({
          hits: 1,
          misses: 2
        } as any),
        // DISPATCH ACTION
        merge(this.click$.pipe(mapTo({type: 'MISS'}))),
        // MIDDLEWARE
        tap(action => console.log(action)),
        // REDUCER
        scan((state: any, action: Action) => {
          if (action.type === 'HIT') {
            return {
              hits: state.hits + 1,
              misses: state.misses
            };
          }
          if (action.type === 'MISS') {
            return {
              hits: state.hits,
              misses: state.misses + 1
            };
          }
        })
      );
  }

  ngOnInit() {
  }

  @HostListener('click')
  handleClick() {
    this.click$.next();
  }

}
