import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { ByHandComponent } from './by-hand/by-hand.component';
import { FieldComponent } from './field/field.component';
import { LetterComponent } from './letter/letter.component';
import { PointsComponent } from './points/points.component';
import { CannonComponent } from './cannon/cannon.component';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';


@NgModule({
  declarations: [
    AppComponent,
    ByHandComponent,
    FieldComponent,
    LetterComponent,
    PointsComponent,
    CannonComponent
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    !environment.production ? StoreDevtoolsModule.instrument() : []
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
