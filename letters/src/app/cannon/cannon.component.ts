import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-cannon',
  templateUrl: './cannon.component.html',
  styleUrls: ['./cannon.component.css']
})
export class CannonComponent {
  keyPressed: string;

  constructor() { }

  @HostListener('document:keyup', ['$event'])
  handleKey($event: KeyboardEvent) {
    this.keyPressed = $event.key;
  }

}
