import { Component, OnInit, Input, HostBinding, OnChanges } from '@angular/core';

@Component({
  selector: 'app-letter',
  templateUrl: './letter.component.html',
  styleUrls: ['./letter.component.css']
})
export class LetterComponent implements OnChanges {

  @Input() value: string;
  @Input() x: number;
  @Input() y: number;

  @HostBinding('style.transform')
  transform = '';

  constructor() { }

  ngOnChanges() {
    this.transform = `translate(${this.x * 20}px, ${this.y * 20}px)`;
  }

}
