import { Action } from '@ngrx/store';
import { Letter } from '../reducers/letters.reducer';

export enum LettersActionTypes {
  Create = '[Letters] Create',
  Destroy = '[Letters] Destroy',
  Move = '[Letters] Move'
}

export class Create implements Action {
  readonly type = LettersActionTypes.Create;

  constructor(public payload: Letter) {
  }
}

export class Destroy implements Action {
  readonly type = LettersActionTypes.Destroy;

  constructor(public payload: number) {
  }
}

export class Move implements Action {
  readonly type = LettersActionTypes.Move;
}

export type LettersActions = Create | Destroy | Move;
