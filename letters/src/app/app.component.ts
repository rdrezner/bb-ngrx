import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from './reducers';
import { Observable } from 'rxjs/Observable';
import { Letter } from './reducers/letters.reducer';
import { timer } from 'rxjs/observable/timer';
import { map, withLatestFrom, tap } from 'rxjs/operators';
import { Create, Move, Destroy } from './actions/letters.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  letters$: Observable<Letter[]>;

  constructor(private store: Store<State>) {
    this.letters$ = store.select('letters');

    timer(0, 2000)
      .pipe(map(this.randomLetter))
      .subscribe(letter => this.store.dispatch(new Create(letter)));

    timer(0, 1500)
      .pipe(
        withLatestFrom(this.store.select('letters')),
        map(([, letters]) => letters),
        tap(letters => this.checkForLosses(letters))
      )
      .subscribe(() => this.store.dispatch(new Move()));
  }

  private randomLetter(): Letter {
    const id = Math.random();
    const y = 1;
    const x = Math.floor(Math.random() * 40);
    const value = String.fromCharCode(Math.floor(Math.random() * 26) + 97);
    return { id, x, y, value };
  }

  private checkForLosses(letters: Letter[]) {
    letters.forEach(letter => {
      if (letter.y > 15) {
        this.store.dispatch(new Destroy(letter.id));
      }
    });
  }
}
